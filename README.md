### Ensure you set the source directory inside the Vagrantfile 

*  Edit ```Vagrantfile``` and set the directory you cloned acetate-project to, relative to your home directory (if applicable):

--
```
config.vm.synced_folder "/PATH/TO/WHERE/YOU/CLONED/acetate-project/", "/srv/www/acetate"
```
